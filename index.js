// console.log("Add post")

let posts=[];
let count=1;


// Add post data
// addEventlistener(event,callback function that will be triggered if the event occur)
document.querySelector("#form-add-post").addEventListener('submit', (event) => {
	// prevent dafault function stops the auto reload of the webpage when submitting
	event.preventDefault(); 
	posts.push({
		id:count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	count++;//increment object id
	showPosts(posts);
	alert("Successfully added");	
});

const showPosts = (posts)=>{
	let postEntries="";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// Edit Post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
};


document.querySelector("#form-edit-post").addEventListener('submit', (event) => {
	event.preventDefault();
	const idToBeEdited=document.querySelector("#txt-edit-id").value;

	for(let i = 0; i < posts.length; i++){
		if (posts[i].id.toString() === idToBeEdited) {
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Edit is successful");

			break; 
		}


	}

});
// Activity s48
/*const deletePost = (id) => {
	posts = posts.filter((post) => {
		if (post.id.toString() !== id) {
			return post;


		}

	})
document.querySelector(`#post-${id}`).remove();
}*/
// Activity s48
const deletePost = (id) => {
  posts=posts.splice(id - 1, 1)
  console.log("deleted", id);
  document.querySelector(`#post-${id}`).remove();
}


